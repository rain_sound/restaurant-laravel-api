<?php

use Faker\Generator as Faker;

$factory->define(App\Dish::class, function (Faker $faker) {
    return [
        'category_id' => $faker->numberBetween($min = 0, $max = 1),
        'name_dish' => $faker->word,
        'img_dish' => $faker->imageUrl($width = 200, $height = 300)
    ];
});
