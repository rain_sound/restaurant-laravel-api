<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
      // Probar si cuando existe una relacion se necesita
      //poner esta variable o no
      'user_id' => $faker->numberBetween($min = 1, $max = 4),
      'order_date' =>$faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
