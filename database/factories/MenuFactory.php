<?php

use Faker\Generator as Faker;

$factory->define(App\Menu::class, function (Faker $faker) {
    return [
      'desc_menu' => $faker->word,
      'date_menu' => $faker->date($format = 'Y-m-d', $max = 'now')
    ];
});
