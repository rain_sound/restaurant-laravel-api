<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name_user' => $faker->name,
        'email_user' => $faker->unique()->safeEmail,
        'pass_user' => $faker->password,
        'address_user' => $faker->streetAddress,
        'type_user' => $faker->numberBetween(0,1)
    ];
});
