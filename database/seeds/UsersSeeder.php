<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
      /*
      //Crear objetos fakes
      $faker = \Faker\Factory::create();

      for($i = 0; $i < 1; $i++){
        //Cuidado con las referencias App a la clase
        App\User::create([
           'nom_usu' => $faker->name,
           'email_usu' => $faker->email,
           'pass_usu' => $faker->password,
           'dire_usu'=> $faker->address,
           'tipo_usu'=> $faker->numberBetween($min=0, $max=1)
        ]);
        */
        factory(App\User::class,4)
          ->create()
          ->each( function($u) {
            $u->orders()->save(factory(App\Order::class)->make());
          });
    }
}
