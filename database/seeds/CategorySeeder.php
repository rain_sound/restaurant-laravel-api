<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Crea mas de un modelo (instancia) por lo cual se usa create
        //que devuelve una coleccion despues para cada modelo creado
        //se crean 4 instancias con el metodo make que crea pero no guarda
        //por eso se usa save de eloquent, la relacion se hace explicita
        //en category->dishes
          factory(App\Category::class, 2)
            ->create()
            ->each(function($category){
              $category->dishes()->save(factory(App\Dish::class)->make());
          });
    }
}
