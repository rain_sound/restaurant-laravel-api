<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   protected $fillable = [
     'user_id',
     'order_date'
   ];

   public function user(){
     //Se referencia al modelo - lo demas lo hace Eloquent
     return $this->belongsTo('App\User');
   }
}
