<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
      protected $fillable = [
        'category_id',
        'name_dish',
        'img_dish',
      ];

      public function category(){
        return $this->belongsTo('App\Category');
      }
}
