<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
      'desc_category',
    ];

    public function dishes(){
      return $this->hasMany('App\Dish');
    }
}
