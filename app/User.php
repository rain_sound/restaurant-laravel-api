<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  protected $fillable = [
    'name_user',
    'email_user',
    'pass_user',
    'address_user',
    'type_user',
 ];

 public function orders(){
   return $this->hasMany('App\Order');
 }
}
