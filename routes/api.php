<?php

use Illuminate\Http\Request;
use App\User;

Route::apiResource('users','UserController');
Route::apiResource('categories',"CategoryController");
Route::apiResource('dishes','DishController');
Route::apiResource('menus','MenuController');
Route::apiResource('orders','OrderController');
Route::get('categories/dishes/{id}','CategoryController@showByCategory');
Route::get('users/{id}/orders','UserController@showByUser');
